import os, sys , random
import pygame , time
from pygame.locals import *
pygame.init()
    
    


class Intro:
    def __init__(self):
        self.display_width = 900
        self.display_height = 700
        self.Display = pygame.display.set_mode((self.display_width,self.display_height))
        self.State = 'First State'
        self.HeroX = 0
        self.HeroY = 0
        self.numFriend = 0
        self.FireX = 0
        self.FireY = 0
        self.HandX = random.randrange(0,self.display_width-50,50)
        self.HandY = random.randrange(0,self.display_height-50,50)
        self.FriendX = random.randrange(0,self.display_width-50,50)
        self.FriendY = random.randrange(0,self.display_height-50,50)
        self.ZombieX = random.randrange(0,self.display_width-100,50)
        self.ZombieY = random.randrange(0,self.display_height-100,50)
        self.PeopleX = random.randrange(0,self.display_width-100,50)
        self.PeopleY = random.randrange(0,self.display_height-100,50)              
        self.num = random.randrange(0,1)
        self.Direction = ['Left','Right','Up','Down']
        self.Direct = 'Right'
        self.zombie = self.Direction[self.num]        
        self.herolist = []
        self.lstHero = []  
        self.heroLength = 0
        self.clock = pygame.time.Clock()
        self.AllScore1 = 0
        self.AllScore2 = 0
        
    def IntroScreen(self):
        pygame.display.set_caption('HERO!')
        clock = pygame.time.Clock()    
        BGScreen = pygame.image.load('BG.jpg')
        intro = True
        while intro:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_p:
                        self.Start()
                        self.mainLoop()
                    if event.key == pygame.K_q:
                        pygame.quit()
                        quit()     
            self.Display.blit(BGScreen,(0,0))
            pygame.display.update()  
    def message(self,size,text,color,position):
        fontObj = pygame.font.Font('freesansbold.ttf', size)
        textSurfaceObj = fontObj.render(text, True, color)
        textRectObj = textSurfaceObj.get_rect()
        textRectObj.center = position     
        
        self.Display.blit(textSurfaceObj, textRectObj)
        
    def Start(self):
        self.Display.fill((0,0,0))
        HowTo = pygame.image.load('how to play.jpg')
        self.Display.blit(HowTo,(0,0))
        pygame.display.update()
        time.sleep(2)
        
        score = pygame.image.load('GetScore.jpg')
        self.Display.blit(score,(-10,0))
        pygame.display.update()
        time.sleep(2)
        
        self.Display.fill((0,0,0))
        self.message(50,'>> READY?? <<',(255,255,255),((self.display_width//2),(self.display_height//2)))
        pygame.display.update()
        time.sleep(1)
          
        self.Display.fill((0,0,0))
        self.message(100,'3',(255,255,255),((self.display_width//2),(self.display_height//2)))
        pygame.display.update()
        time.sleep(1)      
        self.Display.fill((0,0,0))
        
        self.message(100,'2',(255,255,255),((self.display_width//2),(self.display_height//2)))
        pygame.display.update()
        time.sleep(1)   
        self.Display.fill((0,0,0))
        
        self.message(100,'1',(255,255,255),((self.display_width//2),(self.display_height//2)))
        pygame.display.update()
        time.sleep(1)     
        

    def mainLoop(self):
        Hero = pygame.image.load('MYHERO.png')
        FirstScreen = pygame.image.load('grassBG2.jpg')
        SecondScreen = pygame.image.load('wallpaper.jpg')
        Hand = pygame.image.load('zombiesLogo.png')
        Friend = pygame.image.load('Helper.png')
        Zombie = pygame.image.load('zombie.png')
        People = pygame.image.load('Friend.png') 
        ZombieList = pygame.sprite.Group()
        AttackList = pygame.sprite.Group()
        
        

        x_change = 0
        y_change = 0
        x_fire = 0
        y_fire = 0          
        gameExit = False
        while not gameExit:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    gameExit = True  
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        x_change = -50
                        y_change = 0
                        self.Direct = 'Left'
                    elif event.key == pygame.K_RIGHT:
                        x_change = 50
                        y_change = 0
                        self.Direct = 'Right'
                    elif event.key == pygame.K_UP:
                        y_change = -50
                        x_change = 0
                        self.Direct = 'Up'
                    elif event.key == pygame.K_DOWN:
                        y_change = 50
                        x_change = 0
                        self.Direct = 'Down'     
                    elif event.key == pygame.K_SPACE: 
                        self.FireX = self.HeroX
                        self.FireY = self.HeroY                   
                        if self.Direct == 'Left':
                            while self.FireX > 0 :
                                pygame.draw.circle(self.Display,(255,255,0),(self.FireX,self.FireY+25),5)
                                self.FireX -= 10
                                pygame.display.update()
                                self.clock.tick(100000)
                           
                        if self.Direct == 'Right':
                            while self.FireX < self.display_width:
                                pygame.draw.circle(self.Display,(255,255,0),(self.FireX+50,self.FireY+25),5)
                                self.FireX += 10
                                pygame.display.update()
                                self.clock.tick(100000)
                        if self.Direct == 'Up':
                            while self.FireY > 0 :
                                pygame.draw.circle(self.Display,(255,255,0),(self.FireX+25,self.FireY),5)
                                self.FireY -= 10
                                pygame.display.update()
                                self.clock.tick(100000) 
                        if self.Direct == 'Down':
                            while self.FireY < self.display_height:
                                pygame.draw.circle(self.Display,(255,255,0),(self.FireX+25,self.FireY+50),5)
                                self.FireY += 10
                                pygame.display.update()
                                self.clock.tick(100000)
                        ZombieFire = Fire(self.ZombieX,self.ZombieY,self.Display,self.display_width,self.display_height)    
                        ZombieFire.Fire_Zombie()                        
            
            
                        
            self.HeroX += x_change
            self.HeroY += y_change  
            
            self.crashWall()
            self.crashHand()
            self.crashZombie()
            self.crashPeople()
            self.crashFire()
            self.AddFriend()
            self.Moving_Zombie()                  
            
            self.Display.fill((255,156,64))
            if self.State == 'First State':
                self.Display.blit(FirstScreen,(0,0))
            if self.State == 'Second State':
                self.Display.blit(SecondScreen,(0,0))
            self.Display.blit(Hand,(self.HandX,self.HandY))   
            
            if self.numFriend < 5:
                self.Display.blit(Friend,(self.FriendX,self.FriendY))
            self.Display.blit(Zombie,(self.ZombieX,self.ZombieY))
            self.Display.blit(People,(self.PeopleX,self.PeopleY))
            

            
            for XnY in self.herolist:
                self.Display.blit(Friend,(XnY[0],XnY[1])) 
            if self.State == 'First State':
                self.ShowScore1()
                if self.AllScore1 > 10:
                    self.State = 'Second State'
                    self.Display.fill((255,255,255))
                    self.message(100,'>> NEXT <<',(0,0,0),((self.display_width//2),(self.display_height//2)))
                    pygame.display.update()
                    time.sleep(1)  
                    self.mainLoop()
            if self.State == 'Second State':
                self.ShowScore2()
                if self.AllScore2 > 10:
                    self.YouWin()         
                
                
            self.Display.blit(Hero,(self.HeroX,self.HeroY))
                        
            
            pygame.display.update()  
            self.clock.tick(5)
    def Fire_Zombie(self):
        a = self.Direction[self.Fire]
        Zombie_fireX = self.ZombieX
        Zombie_fireY = self.ZombieY        
        if a == 'Left':
            while Zombie_fireX > 0 :
                pygame.draw.circle(self.Display,(255,0,0),(Zombie_fireX,Zombie_fireY+25),5)
                Zombie_fireX -= 10
                pygame.display.update()
                self.clock.tick(100000) 
        if a == 'Right':
            while Zombie_fireX < self.display_width:
                pygame.draw.circle(self.Display,(255,0,0),(Zombie_fireX+50,Zombie_fireY+25),5)
                Zombie_fireX += 10
                pygame.display.update()
                self.clock.tick(100000) 
        if a == 'Up':
            while Zombie_fireY > 0 :
                pygame.draw.circle(self.Display,(255,0,0),(Zombie_fireX+25,Zombie_fireX),5)
                Zombie_fireY -= 10
                pygame.display.update()
                self.clock.tick(100000)  
        if a == 'Down':
            while Zombie_fireY < self.display_height:
                pygame.draw.circle(self.Display,(255,0,0),(Zombie_fireX+25,Zombie_fireY+50),5)
                Zombie_fireY += 10
                pygame.display.update()
                self.clock.tick(100000)                            
    
    
    def YouWin(self):
        win = pygame.image.load('WinBg.jpg')
        Winning = True
        while Winning == True:
            self.message(50,'press Q to quit',(0,0,255),(900//2,600))
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        pygame.quit()
                        quit()                        
            self.Display.blit(OverBG,(0,0))        

    def Moving_Zombie(self):
        if self.zombie == 'Left':
            self.ZombieX -= 50
            self.ZombieY += 0
                         
        elif self.zombie == 'Right':
            self.ZombieX += 50
            self.ZombieY += 0
                           
        elif self.zombie == 'Up':
            self.ZombieY -= 50
            self.ZombieX += 0
                            
        elif self.zombie == 'Down':
            self.ZombieY += 50
            self.ZombieX += 0
                            
        if self.ZombieX < 0 or self.ZombieX > self.display_width - 50:
            self.num = random.randrange(0,4)
            self.Direction = ['Left','Right','Up','Down']
            self.zombie = self.Direction[self.num]         
        if self.ZombieY < 0 or self.ZombieY > self.display_height - 50:
            self.num = random.randrange(0,4)
            self.Direction = ['Left','Right','Up','Down']
            self.zombie = self.Direction[self.num]               
    
    def crashWall(self):
        if self.HeroX + 50 > self.display_width or self.HeroX < 0 or self.HeroY + 50 > self.display_height or self.HeroY < 0:
            self.gameOver()        
    def crashHand(self):
        if self.HeroX == self.HandX and self.HeroY == self.HandY :
            self.gameOver()
    def crashZombie(self):
        if self.HeroX == self.ZombieX and self.HeroY == self.ZombieY:
            self.gameOver()
    def crashPeople(self):
        if self.HeroX == self.PeopleX and self.HeroY == self.PeopleY:
            if self.State == 'First State':
                self.AddScore1(0.25)
            if self.State == 'Second State':
                self.AddScore2(0.25)            
            self.PeopleX = random.randrange(0,self.display_width-100,50)
            self.PeopleY = random.randrange(0,self.display_height-100,50)            
    def crashFire(self):
        if (self.FireX == self.HandX and self.FireY == self.HandY + 25 )or (self.FireX == self.HandX + 25 and self.FireY == self.HandY) or (self.FireX == self.HandX + 50 and self.FireY == self.HandY + 25) or (self.FireX == self.HandX + 25 and self.FireY == self.HandY + 50):
            self.HandX = random.randrange(0,self.display_width-100,50)
            self.HandY = random.randrange(0,self.display_height-100,50)
            if self.State == 'First State':
                self.AddScore1(1)
            if self.State == 'Second State':
                self.AddScore2(1)

    
    
    
    def AddFriend(self):
        herohead = []
        herohead.append(self.HeroX)
        herohead.append(self.HeroY)
        self.herolist.append(herohead)  
        if len(self.herolist) > self.heroLength:
            del self.herolist[0]
        for eachSegment in self.herolist[:-1]:
            if eachSegment == herohead:
                self.gameOver()
        
        if self.HeroX == self.FriendX and self.HeroY == self.FriendY:        
            self.FriendX = random.randrange(0,self.display_width-50,50)
            self.FriendY = random.randrange(0,self.display_height-50,50)   
            self.heroLength += 1
            self.numFriend = self.numFriend + 1            
            if self.State == 'First State':
                self.AddScore1(2)
            if self.State == 'Second State':
                self.AddScore2(2)
            
            
            
    def gameOver(self):
        gameOver = True
        OverBG = pygame.image.load('gameover.jpg')
        while gameOver:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        pygame.quit()
                        quit()
                                     
            self.Display.blit(OverBG,(0,0))
            pygame.display.update() 
            

        
            
    def AddScore1(self,score):
        self.AllScore1 += score
    def ShowScore1(self):
        smallfont = pygame.font.SysFont("comicsansms",25)
        word = smallfont.render("Score: "+str(self.AllScore1)+'/10',True,(0,0,0))
        self.Display.blit(word,[700,30]) 
    def AddScore2(self,score):
        self.AllScore2 += score
    def ShowScore2(self):
        smallfont = pygame.font.SysFont("comicsansms",25)
        word = smallfont.render("Score: "+str(self.AllScore2)+'/10',True,(0,0,0))
        self.Display.blit(word,[700,30])     

            
class Fire:
    def __init__(self,X,Y,screen,width,height):
        self.Zombie_fireX = X
        self.Zombie_fireY = Y
        self.Display = screen
        self.width = width
        self.height = height
        self.Direction2 = ['Left','Right','Up','Down']
        self.Fire = random.randrange(0,3)
        self.clock = pygame.time.Clock()
        
        
    def Fire_Zombie(self):
        a = self.Direction2[self.Fire]
        if a == 'Left':
            while self.Zombie_fireX > 0 :
                pygame.draw.circle(self.Display,(255,0,0),(self.Zombie_fireX,self.Zombie_fireY+25),5)
                self.Zombie_fireX -= 10
                pygame.display.update()
                self.clock.tick(100000) 
        if a == 'Right':
            while self.Zombie_fireX < self.width:
                pygame.draw.circle(self.Display,(255,0,0),(self.Zombie_fireX+50,self.Zombie_fireY+25),5)
                self.Zombie_fireX += 10
                pygame.display.update()
                self.clock.tick(100000) 
        if a == 'Up':
            while self.Zombie_fireY > 0 :
                pygame.draw.circle(self.Display,(255,0,0),(self.Zombie_fireX+25,self.Zombie_fireX),5)
                self.Zombie_fireY -= 10
                pygame.display.update()
                self.clock.tick(100000)  
        if a == 'Down':
            while self.Zombie_fireY < self.height:
                pygame.draw.circle(self.Display,(255,0,0),(self.Zombie_fireX+25,self.Zombie_fireY+50),5)
                self.Zombie_fireY += 10
                pygame.display.update()
                self.clock.tick(100000)  
if __name__ == '__main__':
    a = Intro()
    a.IntroScreen()
